import os
CYAN = '\033[36m'
RED = '\033[31m'
GREEN = '\033[32m'
YELLOW = '\033[33m'
RESET = '\033[39m'


# Función de edición del diccionario
def editar(opcion, directorio):
    volver = 'nada'
    os.system('clear')
    while volver.upper() != 'VOLVER':
        print("")
        if opcion.upper() == 'AGREGAR':
            print(CYAN+"<<<<<<<<<< AGREGAR ELEMENTOS >>>>>>>>>>")
            key = input(RESET+"Escriba el código de la proteína que desea agregar: ")
            nombre = input("Escriba el nombre de la proteína: ")
            if key in directorio:
                os.system('clear')
                print(RED+"Ninguna acción realizada: el código de esta proteína ya está en el directorio", end=RESET+"\n")
            else:
                directorio[key] = nombre
                os.system('clear')
                print(GREEN+"Proteína agregada correctamente", end=RESET+"\n")
        elif opcion.upper() == 'BORRAR':
            print(CYAN+"<<<<<<<<<< BORRAR ELEMENTOS >>>>>>>>>>\n", end=RESET+"")
            key = input("Escriba el código del elemento que desea borrar: ")
            if key in directorio:
                del(directorio[key])
                os.system('clear')
                print(GREEN+"Elemento borrado correctamente", end=RESET+"\n")
            else:
                os.system('clear')
                print(RED+"El código ingresado no se encuentra en el directorio", end=RESET+"\n")
        elif opcion.upper() == 'EDITAR':
            print(CYAN+"<<<<<<<<<< EDITAR ELEMENTOS >>>>>>>>>>\n", end=RESET+"\n")
            key = input("Escriba el código de la proteína que desea editar: ")
            nombre = input("Escriba el nombre que desea asignar a esta proteína: ")
            if key in directorio:
                directorio[key] = nombre
                os.system('clear')
                print(GREEN+"Proteína editada correctamente", end=RESET+"\n")
            else:
                os.system('clear')
                print(RED+"El código ingresado no se encuentra en el directorio", end=RESET+"\n")
        print(CYAN+"<<<<<<<<<< ¿? >>>>>>>>>>", end=RESET+"\n")
        print("Si desea volver al menú principal, escriba 'volver'")
        print("Si desea continuar en este modo, escriba 'continuar'")
        volver = input()
        if volver.upper() == 'VOLVER':
            os.system('clear')
            print("")
            pass
        elif volver.upper() == 'CONTINUAR':
            print("")
            os.system('clear')
            pass
        else:
            os.system('clear')
            print(RED+"Orden ingresada inválida", end=RESET+"")
    return(directorio)


# Funcion de impresión del diccionario
def pregunta(opcion, directorio):
    os.system('clear')
    if opcion.upper() == 'CONSULTA':
        print("")
        key = input(CYAN+"Ingrese el código de la proteína que desea consultar: ")
        print(RESET+"")
        if key in directorio:
            os.system('clear')
            print(GREEN+"Mostrando proteína consultada", end=RESET+"\n")
            print("Código:", key, "-> '", directorio[key], "'")
        else:
            os.system('clear')
            print(RED+"El código ingresado no se encuentra en el directorio", end=RESET+"\n")
    elif opcion.upper() == 'IMPRIMIR':
        print(GREEN+"Imprimiendo directorio", end=RESET+"\n")
        for key, item in directorio.items():
            print(f"Código: {key} -> '{item}'")
        print(GREEN+"Fin impresión del directorio", end=RESET+"\n")


# Función de reinicio del directorio
def reset(directorio):
    remove = []
    os.system('clear')
    print("")
    print(CYAN+"<<<<<<<<<< Reinicio >>>>>>>>>>", end=RESET+"\n")
    print(YELLOW+"¿Está seguro que desea reiniciar el directorio?", end=RESET+"\n")
    print("Escriba 'si' para borrar todos los datos del directorio")
    print("Escriba 'no' para NO borrar los datos")
    borrar = input()
    if borrar.upper() == 'SI':
        for key, value in directorio.items():
            remove.append(key)
        for key in remove:
            del directorio[key]
        os.system('clear')
        print(GREEN+"Directorio borrador correctamente", end=RESET+"\n")
    elif borrar.upper() == 'NO':
        os.system('clear')
        print(GREEN+"Ningún cambio realizado", end=RESET+"\n")
    else:
        os.system('clear')
        print(RED+"Orden ingresada inválida", end=RESET+"\n")


# Funcion Principal
os.system('clear')
directorio = {"6S1A": "Ligand binding domain of the P. putida receptor PcaY_PP",
              "6S18": "Ligand binding domain of the P. putida receptor PcaY_PP in complex with glycerol",
              "6S33": "Ligand binding domain of the P. putida receptor PcaY_PP in complex with Protocatechuate",
              "6S37": "Ligand binding domain of the P. putida receptor PcaY_PP in complex with salicylic acid"}
opcion = 'nada'


# Opciones del Menú Principal
print("")
while (opcion.upper() != 'SALIR'):
    print(CYAN+"<<<<<<<<<< MENÚ PRINCIPAL >>>>>>>>>>", end=RESET+"\n")
    print("Para agregar un elemento al diccionario, escriba 'agregar'")
    print("Para borrar un elemento del diccionario, escriba 'borrar'")
    print("Para editar un elemento del diccionario, escriba 'editar'")
    print("Para consultar por un elemento del diccionario, escriba 'consulta'")
    print("Para obtener todos los elementos del diccionario, escriba 'imprimir'")
    print("Para reiniciar el directorio de proteínas, escriba 'reset'")
    print("Para salir, escriba 'salir'")
    opcion = input("Esciba una opción a continuación: ")
    if opcion.upper() == 'AGREGAR' or opcion.upper() == 'BORRAR' or opcion.upper() == 'EDITAR':
        directorio = editar(opcion, directorio)
    elif opcion.upper() == 'CONSULTA' or opcion.upper() == 'IMPRIMIR':
        pregunta(opcion, directorio)
    elif opcion.upper() == 'RESET':
        reset(directorio)
    else:
        os.system('clear')
        print(RED+"Orden ingresada inválida", end=RESET+"\n")

os.system('clear')
print(GREEN+"Programa cerrado correctamente\n", end=RESET+"")

