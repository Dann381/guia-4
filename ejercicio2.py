import os
CYAN = '\033[36m'
RED = '\033[31m'
GREEN = '\033[32m'
YELLOW = '\033[33m'
RESET = '\033[39m'


# Función de edicion del diccionario
def entrada(dic):
    bucle = 'nada'
    os.system('clear')
    print("")
    while bucle.upper() != 'VOLVER':
        print(CYAN+"<<<<<<<<<< Edición del diccionario >>>>>>>>>>")
        print(RESET+"Para agregar una palabra, escriba 'agregar'")
        print("Para borrar una palabra, escriba 'borrar'")
        print("Para editar una palabra, escriba 'editar'")
        opcion = input()
        if opcion.upper() == 'AGREGAR':
            os.system('clear')
            print("")
            print(CYAN+"<<<<<<<<<< Agregar palabra >>>>>>>>>>")
            key = input(RESET+"Escriba la palabra en español: ")
            if key.lower() in dic:
                os.system('clear')
                print(RED+"La palabra ya se encuentra en el diccionario")
            else:
                value = input(RESET+"Escriba la palabra en ingles: ")
                dic[key.lower()] = value.lower()
                os.system('clear')
                print(GREEN+"Palabra agregada correctamente")
        elif opcion.upper() == 'BORRAR':
            os.system('clear')
            print("")
            print(CYAN+"<<<<<<<<<< Borrar palabra >>>>>>>>>>")
            key = input(RESET+"Escriba la palabra (en español) que desea borrar: ")
            if key.lower() in dic:
                del(dic[key.lower()])
                os.system('clear')
                print(GREEN+"Palabra borrada correctamente")
            else:
                os.system('clear')
                print(RED+"La palabra ingresada no se encuentra en el diccionario")
        elif opcion.upper() == 'EDITAR':
            os.system('clear')
            print("")
            print(CYAN+"<<<<<<<<<< Editar palabra >>>>>>>>>>")
            key = input(RESET+"Ingrese la palabra (en español) que desea editar: ")
            if key.lower() in dic:
                value = input("Ingrese la palabra en inglés: ")
                dic[key.lower()] = value.lower()
                os.system('clear')
                print(GREEN+"Palabra editada correctamente")
            else:
                os.system('clear')
                print(RED+"La palabra ingresada no se encuentra en el diccionario")

        else:
            os.system('clear')
            print(RED+"Orden ingresada no válida")
        print(CYAN+"<<<<<<<<<< ¿? >>>>>>>>>>")
        print(RESET+"Si desea continuar en este modo, escriba 'continuar'")
        print("Si desea volver al menú, escriba 'volver'")
        bucle = input()
        if bucle.upper() == 'CONTINUAR':
            os.system('clear')
            print("")
            pass
        elif bucle.upper() == 'VOLVER':
            os.system('clear')
            pass
        else:
            os.system('clear')
            print(RED+"Orden ingresada no válida")
    return dic


# Funcion de traduccion
def traducir(dic):
    lista = []
    traduccion = []
    os.system('clear')
    print("")
    print(CYAN+"<<<<<<<<<< Traducción >>>>>>>>>>")
    print(RESET+"Ingrese una frase a continuación:")
    frase = input()
    minusculas = frase.lower()
    lista = minusculas.split()
    for palabra in lista:
        if palabra in dic:
            traduccion.append(dic[palabra])
        else:
            traduccion.append(palabra)
    for palabra in traduccion:
        print(palabra, end=" ")
    print("")


# Función para mostrar el diccionario
def mostrar(dic):
    os.system('clear')
    print(CYAN+"<<<<<<<<<< Mostrando diccionario >>>>>>>>>>", end=RESET+"\n")
    for key, item in dic.items():
        print(f"'{key}' ---> '{item}'")
    print(CYAN+"<<<<<<<<<< Fin de impresión del diccionario >>>>>>>>>>", end=RESET+"\n")


# Función principal
dic = {'hola': 'hello', 'cómo': 'how', 'estás': 'are you',
       'bien': 'fine', 'chao': 'bye', 'adiós': 'bye',
       'mundo': 'world', 'y': 'and', 'tu': 'you',
       'mal': 'bad', 'estás?': 'are you?', 'entiendo': 'understand'}
salir = 'nada'
os.system('clear')
print("")
while salir.upper() != 'SALIR':
    print(CYAN+"<<<<<<<<<< Menú principal >>>>>>>>>>")
    print(RESET+"Si desea editar las palabras del diccionario, escriba 'entrada'")
    print("Si desea traducir una frase, escriba 'traducir'")
    print("Si desea ver las palabras que hay en el diccionario, escriba 'mostrar'")
    print("Si desea salir, escriba 'salir'")
    salir = input()
    if salir.upper() == 'ENTRADA':
        dic = entrada(dic)
    elif salir.upper() == 'TRADUCIR':
        traducir(dic)
    elif salir.upper() == 'MOSTRAR':
        mostrar(dic)
    elif salir.upper() == 'SALIR':
        os.system('clear')
    else:
        os.system('clear')
        print(RED+"Orden ingresada no válida")
os.system('clear')
print(CYAN+"<<<<<<<<<< Programa terminado >>>>>>>>>>\n")
